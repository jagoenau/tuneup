package se1app.applicationcore.interaction;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class MessageRepositoryTest {
	public MessengerRepository messengerRepository;
	public MessageRepository messageRepository;
	public Messenger messengerKaiFoo;
	
	@Before
	public void setUp() {
		User kai = new User("Kai", new EmailType("kai.info@gmx.de"), new PasswordType("ABDadSbc12"));
		User foo = new User("Foo", new EmailType("foo.info@gmx.de"), new PasswordType("ABDadSbc12"));
		Set<User> users = new HashSet<User>(Arrays.asList(kai, foo));
		messengerKaiFoo = new Messenger(users);
		messengerRepository.save(messengerKaiFoo);
		Message message1 = new Message(kai, foo, "hello");
		Message message2 = new Message(foo, kai, "hi");
		messengerKaiFoo.addMessage(message1);
		messengerKaiFoo.addMessage(message2);
		messageRepository.save(message1);
		messageRepository.save(message2);
	}

	@Test
	public void testFindByMessenger() {
		List<Message> messages = messageRepository.findByMessenger(messengerKaiFoo);
		assertThat(messages.size()==2);
	}

}
