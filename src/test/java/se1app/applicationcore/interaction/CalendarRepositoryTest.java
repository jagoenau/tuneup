package se1app.applicationcore.interaction;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class CalendarRepositoryTest {
	Event cocktail;
    
    @Autowired
    private CalendarRepository calendarRepository;
    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private UserRepository userRepository;
    
    @Before
    public void setup() {
        cocktail = new Event("Cocktail", "Madison Street");
        Event teddy = new Event("teddy bear", "toy store");
        eventRepository.save(cocktail);
        eventRepository.save(teddy);        
        User kai = new User("Kai",
            new EmailType("kai.info@gmx.de"), new PasswordType("ABDadSbc12"));
        User foo = new User("Foo",
                new EmailType("foo.info@gmx.de"), new PasswordType("ABDadSbc12"));
        userRepository.save(kai);
        userRepository.save(foo);
        Calendar kaisCal = new Calendar(kai);
        kaisCal.addEvent(teddy);
        kaisCal.addEvent(cocktail);
        calendarRepository.save(kaisCal);
    }
    
    @Test
    public void testFindCalenderByEvent() {
        List<Calendar> calendars = calendarRepository.findByEvents(cocktail);
        assertThat(calendars).hasSize(1);
    }
}
