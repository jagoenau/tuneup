package se1app.applicationcore.interaction;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class EventRepositoryTest {
	Invitation kaiFooCocktail;
	Calendar kaisCalendar;

	@Autowired
	private CalendarRepository calendarRepository;
	@Autowired
	private InvitationRepository invitationRepository;
	@Autowired
	private EventRepository eventRepository;

	@Before
	public void setup() {
		User kai = new User("Kai", new EmailType("kai.info@gmx.de"), new PasswordType("ABDadSbc12"));
		User foo = new User("Foo", new EmailType("foo.info@gmx.de"), new PasswordType("ABDadSbc12"));
		Event cocktail = new Event("Cocktail", "Madison Street");
		eventRepository.save(cocktail);
		Event teddy = new Event("teddy bear", "toy store");
		eventRepository.save(teddy);
		kaisCalendar = new Calendar(kai);
		calendarRepository.save(kaisCalendar);
		kaiFooCocktail = new Invitation(cocktail, kai, foo);
		invitationRepository.save(kaiFooCocktail);
	}

	@Test
	public void testFindEventByInvitation() {
		Event event = eventRepository.findByInvitations(kaiFooCocktail);
		assertThat(event.getDescription().equals("Cocktail"));
	}

	 @Test
	 public void testFindEventByCalendar() {
	 List<Event> events = eventRepository.findByCalenders(kaisCalendar);
	 assertThat(events.size()==2);
	 }

}