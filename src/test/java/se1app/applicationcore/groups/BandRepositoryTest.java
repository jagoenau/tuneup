package se1app.applicationcore.groups;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.groups.entities.Band;
import se1app.applicationcore.groups.repositories.BandRepository;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class BandRepositoryTest {

	private static final PasswordType TEST_PASSWORD = new PasswordType("ABCabc123");
	@Autowired
	private BandRepository bandRepository;
	@Autowired
	private UserRepository userRepository;
	private User neil;
	private User misterX;

	@Before
	public void setup() {
		List<User> members1 = new ArrayList<User>();
		misterX = new User("MisterX", new EmailType("misterX@test.com"), TEST_PASSWORD);
		userRepository.save(misterX);
		neil = new User("Neil", new EmailType("neil@test.com"), TEST_PASSWORD);
		userRepository.save(neil);
		members1.add(neil);
		Band tDG = new Band("Three Days Grace", members1);
		tDG.addGenre(GenreType.ROCK);
		tDG.addGenre(GenreType.HARDROCK);
		bandRepository.save(tDG);

		List<User> members2 = new ArrayList<User>();
		members1.add(neil);
		members1.add(new User("Edsel", new EmailType("edsel@test.com"), TEST_PASSWORD));
		Band dope = new Band("Dope", members2);
		dope.addGenre(GenreType.METAL);
		bandRepository.save(dope);

		List<User> members3 = new ArrayList<User>();
		members1.add(new User("Mike", new EmailType("mike@test.com"), TEST_PASSWORD));
		members1.add(new User("David", new EmailType("david@test.com"), TEST_PASSWORD));
		Band disturbed = new Band("Disturbed", members3);
		disturbed.addGenre(GenreType.METAL);
		disturbed.addGenre(GenreType.HARDROCK);
		bandRepository.save(disturbed);
	}

	@Test
	public void testFindBandByName() {
		assertEquals(1, bandRepository.findByNameIgnoreCaseContaining("Three Days Grace").size());
		assertEquals(1, bandRepository.findByNameIgnoreCaseContaining("three Days grace").size());
		assertEquals(3, bandRepository.findByNameIgnoreCaseContaining("D").size());
		assertEquals(3, bandRepository.findByNameIgnoreCaseContaining("d").size());
	}

	@Test
	public void testFindBandByGenres() {
		List<GenreType> genres = new ArrayList<>();
		genres.add(GenreType.HARDROCK);
		List<Band> result = bandRepository.findByGenres(genres);
		assertEquals(2, result.size());
	}

	@Test
	public void testFindBandByMembers() {
		List<User> members = new ArrayList<>();
		members.add(neil);
		List<Band> result = bandRepository.findByMembers(members);
		assertEquals(2, result.size());

		members = new ArrayList<>();
		members.add(misterX);
		result = bandRepository.findByMembers(members);
		assertEquals(0, result.size());
	}

}
