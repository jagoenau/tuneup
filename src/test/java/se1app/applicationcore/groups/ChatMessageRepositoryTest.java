package se1app.applicationcore.groups;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.groups.entities.ChatMessage;
import se1app.applicationcore.groups.repositories.ChatMessageRepository;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class ChatMessageRepositoryTest {

	private static final PasswordType TEST_PASSWORD = new PasswordType("ABCabc123");
	@Autowired
	ChatMessageRepository chatMessageRepository;
	@Autowired
	UserRepository userRepository;
	private TimeStampType timestamp;
	private User sender1;
	private User sender2;

	@Before
	public void setUp() {
		timestamp = TimeStampType.now();
		sender1 = new User("sender1", new EmailType("sender1@test.de"), TEST_PASSWORD);
		sender2 = new User("sender2", new EmailType("sender2@test.de"), TEST_PASSWORD);
		userRepository.save(sender1);
		userRepository.save(sender2);

		chatMessageRepository.save(new ChatMessage(sender1, "test1"));
		chatMessageRepository.save(new ChatMessage(sender1, "test2"));
		chatMessageRepository.save(new ChatMessage(sender1, "testcase"));
		chatMessageRepository.save(new ChatMessage(sender1, "this better be test"));
		chatMessageRepository.save(new ChatMessage(sender1, "hey"));

		chatMessageRepository.save(new ChatMessage(sender2, "testcase"));
		chatMessageRepository.save(new ChatMessage(sender2, "blub"));
		chatMessageRepository.save(new ChatMessage(sender2, "blob"));
	}

	@Test
	public void testFindBySender() {
		assertEquals(5, chatMessageRepository.findBySender(sender1).size());

		assertEquals(3, chatMessageRepository.findBySender(sender2).size());
	}

	@Test
	public void testFindByTimeStamp() {
		assertEquals(8, chatMessageRepository.findByTimeStamp(timestamp).size());
	}

	@Test
	public void testFindByContentContaining() {
		assertEquals(5, chatMessageRepository.findByContentContaining("test").size());
		assertEquals(2, chatMessageRepository.findByContentContaining("testcase").size());
		assertEquals(3, chatMessageRepository.findByContentContaining("b").size());
	}
}
