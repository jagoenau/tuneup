package se1app.applicationcore.groups;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.groups.entities.Crowd;
import se1app.applicationcore.groups.repositories.CrowdRepository;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class CrowdRepositoryTest {
	
	private static final PasswordType TEST_PASSWORD = new PasswordType("ABCabc123");
	@Autowired
	CrowdRepository crowdRepository;
	private User emma;
	
	@Before
	public void setup() throws MaximumGroupSizeReachedException {
		List<User> members1 = new ArrayList<>();
		emma = new User("Emma", new EmailType("emma@test.de"), TEST_PASSWORD);
		members1.add(emma);
		members1.add(new User("Tim", new EmailType("tim@test.de"), TEST_PASSWORD));
		members1.add(new User("Jan", new EmailType("jan@test.de"), TEST_PASSWORD));
		members1.add(new User("Emil", new EmailType("emil@test.de"), TEST_PASSWORD));
		
		Crowd crowd1 = new Crowd("Crowd 1", "Test", members1);
		crowdRepository.save(crowd1);
		
		List<User> members2 = new ArrayList<>();
		members2.add(emma);
		members2.add(new User("Dan", new EmailType("dan@test.de"), TEST_PASSWORD));
		members2.add(new User("Tina", new EmailType("tina@test.de"), TEST_PASSWORD));
		members2.add(new User("Joni", new EmailType("joni@test.de"), TEST_PASSWORD));
		
		Crowd crowd2 = new Crowd("Crowd 2", "Test", members2);
		crowdRepository.save(crowd2);
		
	}
	
	@Test
	public void testFindByName(){
		assertEquals(1, crowdRepository.findByNameIgnoreCaseContaining("Crowd 1").size());
		assertEquals(2, crowdRepository.findByNameIgnoreCaseContaining("Crowd ").size());
		assertEquals(2, crowdRepository.findByNameIgnoreCaseContaining("crow").size());
	}
	
	@Test
	public void testFindByMembers(){
		List<User> member = new ArrayList<>();
		member.add(emma);
		List<Crowd> result = crowdRepository.findByMembers(member);
		assertEquals(2, result.size());
	}
	
	@Test
	public void testDelete(){
		List<User> member = new ArrayList<>();
		member.add(emma);
		List<Crowd> result = crowdRepository.findByMembers(member);
		Crowd crowdX = result.get(0);
		crowdX.removeMember(emma);
		crowdRepository.save(crowdX);
		result = crowdRepository.findByMembers(member);
		assertEquals(1, result.size());
		
	}

}
