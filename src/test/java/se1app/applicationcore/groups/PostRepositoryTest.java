package se1app.applicationcore.groups;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.groups.repositories.PostRepository;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.Post;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class PostRepositoryTest {

	private static final PasswordType TEST_PASSWORD = new PasswordType("ABCabc123");
	@Autowired
	PostRepository postRepository;
	@Autowired
	UserRepository userRepository;
	private TimeStampType timestamp;
	private User author1;
	private User author2;

	@Before
	public void setUp() {
		timestamp = TimeStampType.now();
		author1 = new User("author1", new EmailType("author1@test.de"), TEST_PASSWORD);
		author2 = new User("author2", new EmailType("author2@test.de"), TEST_PASSWORD);
		userRepository.save(author1);
		userRepository.save(author2);

		postRepository.save(new Post(author1, "test1"));
		postRepository.save(new Post(author1, "test2"));
		postRepository.save(new Post(author1, "testcase"));
		postRepository.save(new Post(author1, "this better be test"));
		postRepository.save(new Post(author1, "hey"));

		postRepository.save(new Post(author2, "testcase"));
		postRepository.save(new Post(author2, "blub"));
		postRepository.save(new Post(author2, "blob"));
	}

	@Test
	public void testFindByAuthor() {
		assertEquals(5, postRepository.findByAuthor(author1).size());

		assertEquals(3, postRepository.findByAuthor(author2).size());
	}

	@Test
	public void testFindByTimeStamp() {
		assertEquals(8, postRepository.findByTimeStamp(timestamp).size());
	}

	@Test
	public void testFindByContentContaining() {
		assertEquals(5, postRepository.findByContentContaining("test").size());
		assertEquals(2, postRepository.findByContentContaining("testcase").size());
		assertEquals(3, postRepository.findByContentContaining("b").size());
	}
}
