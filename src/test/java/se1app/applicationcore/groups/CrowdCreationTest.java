package se1app.applicationcore.groups;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.groups.entities.Crowd;
import se1app.applicationcore.groups.repositories.CrowdRepository;
import se1app.applicationcore.groups.usecase.CrowdCreation;
import se1app.applicationcore.groups.usecase.CrowdCreationImpl;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.entities.User;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class CrowdCreationTest {

	private static final PasswordType TEST_PASSWORD = new PasswordType("ABCabc123");

	@Autowired
	CrowdRepository crowdRepository;

	private CrowdCreation crowdCreation;
	private Crowd crowd1;

	private User emma;
	private User tim;
	private User jan;
	private List<User> initialInvites;

	@Before
	public void setUp() throws MaximumGroupSizeReachedException {
		crowdCreation = new CrowdCreationImpl(crowdRepository);

		emma = new User("Emma", new EmailType("emma@test.de"), TEST_PASSWORD);
		tim = new User("Tim", new EmailType("tim@test.de"), TEST_PASSWORD);
		jan = new User("Jan", new EmailType("jan@test.de"), TEST_PASSWORD);

		List<User> members1 = new ArrayList<>();
		members1.add(emma);
		members1.add(tim);
		members1.add(jan);
		members1.add(new User("Emil", new EmailType("emil@test.de"), TEST_PASSWORD));

		crowd1 = new Crowd("Crowd 1", "Test", members1);
		crowdRepository.save(crowd1);

		List<User> members2 = new ArrayList<>();
		members2.add(emma);
		members2.add(new User("Dan", new EmailType("dan@test.de"), TEST_PASSWORD));
		members2.add(new User("Tina", new EmailType("tina@test.de"), TEST_PASSWORD));
		User joni = new User("Joni", new EmailType("joni@test.de"), TEST_PASSWORD);
		members2.add(joni);

		Crowd crowd2 = new Crowd("Crowd 2", "Test", members2);
		crowdRepository.save(crowd2);

		initialInvites = Arrays.asList(emma, joni);
	}

	@Test
	public void testCreateCrowd() throws MaximumGroupSizeReachedException {
		crowdCreation.createCrowd(emma, "hello", "tst", initialInvites);
		assertEquals(3, crowdRepository.findAll().size());
	}

	@Test
	public void testGetCrowds() {
		assertEquals(2, crowdCreation.getCrowdsForUser(emma).size());
	}

	@Test
	public void testEditModerators() {
		crowdCreation.addModerators(crowd1, Arrays.asList(emma));
		assertEquals(1, crowd1.getModerators().size());

		crowdCreation.addModerators(crowd1, Arrays.asList(tim, jan));
		crowdCreation.removeModerators(crowd1, Arrays.asList(emma));
		assertEquals(2, crowd1.getModerators().size());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testRemoveModeratorsFail() {
		crowdCreation.addModerators(crowd1,
				Arrays.asList(new User("Thilo", new EmailType("thilo@test.de"), TEST_PASSWORD)));
		assertEquals(1, crowd1.getModerators().size());
	}

}
