package se1app.applicationcore.profile;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.profile.Visibility;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.dataTypes.TagType;
import se1app.applicationcore.profile.entities.Article;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.ArticleRepository;
import se1app.applicationcore.profile.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class ArticleRepositoryTest {
    private User user;
    @Autowired
    private ArticleRepository articleRepository;
    
    @Autowired
    private UserRepository userRepository;
    
    @Before
    public void setup() {
        List<TagType> tags = new ArrayList<>();
        tags.add(new TagType("tag1"));
        tags.add(new TagType("tag2"));
        tags.add(new TagType("tag3"));
        user = new User("TestUser",
            new EmailType("kai.info@gmx.de"), new PasswordType("ABDadSbc12"));
        userRepository.save(user);
        
        Article article01 = new Article("einTitle", tags, "DerInhalt", Visibility.PRIVAT, user);
        articleRepository.save(article01);
        
        article01.addTag(new TagType("addedTag01"));
        
        Article article02 = new Article("Schafe", tags, "Ein Schaf", Visibility.PUBLIC, user);
        articleRepository.save(article02);
        
        article02.addTag(new TagType("addedTag02"));
        article02.addTag(new TagType("addedTag03"));
        
        Article article03 = new Article("Irgendwas", tags, "etwas anderes", Visibility.FRIENDS,
            user);
        articleRepository.save(article03);
        
    }
    
    @Test
    public void testFindAll() {
        List<Article> articles = articleRepository.findAll();
        assertThat(articles).hasSize(3);
    }
    
    @Test
    public void testFindByTitle() {
        Optional<Article> article = articleRepository.findByTitle("Schafe");
        assertThat(article.isPresent());
        assertThat(article.get().getTitle().equals("Schafe"));
        assertThat(article.get().getTags()).hasSize(5);
    }
    
    // @Test
    // public void testFindByTags() {
    // List<Article> articles = articleRepository.findByTags("tag03");
    // assertThat(articles).hasSize(3);
    // }
    
    @Test
    public void testFindByReleaseDateTime() {
        List<Article> articles = articleRepository
            .findByReleaseDateTime(LocalDateTime.of(2016, 11, 14, 12, 55));
        assertThat(articles).hasSize(3);
        
    }
    
    @Test
    public void testFindByAuthor() {
        List<Article> articles = articleRepository.findByAuthor(user);
        assertThat(articles).hasSize(3);
        
    }
    
    @Test
    public void testfindByVisibility() {
        List<Article> articles = articleRepository.findByVisibility(Visibility.PUBLIC);
        assertThat(articles).hasSize(1);
        assertThat(articles.get(0).getTitle()).isEqualTo("Schafe");
    }
    
}
