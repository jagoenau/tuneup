package se1app.applicationcore.profile;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.dataTypes.TagType;
import se1app.applicationcore.profile.entities.Article;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.UserRepository;
import se1app.applicationcore.profile.useCases.CreateArticleUseCase;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class CreateArticleUseCaseTest {
    private CreateArticleUseCase creator01;
    private User user1;
    private List<TagType> tags, failTag;
    private Article testArticle;
    
    @Autowired
    private UserRepository userRepository;
    
    @Before
    public void setup() {
        creator01 = new CreateArticleUseCase();
        tags = new ArrayList<>();
        tags.add(new TagType("tag1"));
        tags.add(new TagType("tag2"));
        tags.add(new TagType("tag3"));
        failTag.add(new TagType("eins"));
        
        user1 = new User("TestUser1",
            new EmailType("kai.info@gmx.de"), new PasswordType("ABDadSbc12"));
        userRepository.save(user1);
        
        testArticle = new Article("einTitle", tags, "DerInhalt",
            Visibility.PRIVAT, user1);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void publishArticleTest() {
        Article article01 = creator01.publishArticle("einTitle", tags, "DerInhalt",
            Visibility.PRIVAT, user1);
        List<Article> l = new ArrayList<>();
        l.add(testArticle);
        assertEquals(l, creator01.getArticlesByUser(user1));
        
        // Exception testing
        creator01.publishArticle(null, tags, "DerInhalt",
            Visibility.PRIVAT, user1);
        creator01.publishArticle("EinTitel", failTag, "DerInhalt",
            Visibility.PRIVAT, user1);
        creator01.publishArticle("einTitel", tags, null,
            Visibility.PRIVAT, user1);
        creator01.publishArticle("einTitel", tags, "Inhalt",
            Visibility.PRIVAT, null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testgetArticlesByUsername() {
        List<Article> articlesOfUser1 = creator01.getArticlesByUser(user1);
        assertThat(articlesOfUser1).hasSize(2);
        
        // Exception testing
        creator01.getArticlesByUser(null);
    }
}
