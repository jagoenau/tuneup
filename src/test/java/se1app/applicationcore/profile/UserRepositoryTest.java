package se1app.applicationcore.profile;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import se1app.applicationcore.Application;
import se1app.applicationcore.profile.Visibility;
import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;
import se1app.applicationcore.profile.dataTypes.TagType;
import se1app.applicationcore.profile.entities.Article;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@ContextConfiguration(classes = Application.class)
public class UserRepositoryTest {
    
    @Autowired
    private UserRepository userRepository;
    
    @Before
    public void setup() {
        List<TagType> tags = new ArrayList<>();
        tags.add(new TagType("tag1"));
        tags.add(new TagType("tag2"));
        tags.add(new TagType("tag3"));
        User hans = new User("Hans",
            new EmailType("hans.peter@lustig.de"), new PasswordType("ABCabc123"));
        hans.addArticle(new Article("einTitle", tags, "DerInhalt", Visibility.PRIVAT, hans));
        hans.addArticle(
            new Article("NocheinTitle", tags, "NocheinInhalt", Visibility.PUBLIC, hans));
        
        userRepository.save(hans);
        
        User kai = new User("Kai",
            new EmailType("kai.info@gmx.de"), new PasswordType("ABDadSbc12"));
        kai.addArticle(new Article("Schafe", tags, "Ein Schaf", Visibility.PUBLIC, kai));
        kai.addArticle(
            new Article("Schafe2", tags, "Noch ein Schaf", Visibility.PRIVAT, kai));
        userRepository.save(kai);
        
        User lisa = new User("Lisa",
            new EmailType("Lisa.lotte@lustig.de"), new PasswordType("ABC14Dabc123"));
        lisa.addArticle(new Article("KeinSchaf", tags, "Kein Schaf", Visibility.PUBLIC, lisa));
        lisa.addArticle(
            new Article("WiederKeinSchaf", tags, "Kein Schaf ", Visibility.PUBLIC, lisa));
        userRepository.save(lisa);
        
    }
    
    @Test
    public void testFindAll() {
        List<User> users = userRepository.findAll();
        assertThat(users).hasSize(3);
    }
    
    @Test
    public void testFindByName() {
        Optional<User> user = userRepository.findByName("Lisa");
        assertThat(user.isPresent());
        assertThat(user.get().getName().equals("Lisa"));
        assertThat(user.get().getArticles()).hasSize(2);
    }
    
}
