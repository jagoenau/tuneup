package se1app.applicationcore.interaction;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import se1app.applicationcore.profile.entities.User;

@Entity
public class Message {
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToOne
	private User sender;
	
	@ManyToOne
	private User receiver;
	private LocalDateTime timeStamp;
	private String content;
	
	private AttachmentType attachment;
    
    @ManyToOne
    private Messenger messenger;
    
    public Message() {}
    
    public Message(User sender, User receiver, String content) {
    	this.sender = sender;
    	this.receiver = receiver;
    	this.content = content;
    	timeStamp = LocalDateTime.now();
    }
    
    public Message(User sender, User receiver, String content, AttachmentType attachment) {
    	this(sender, receiver, content);
    	this.attachment = attachment;
    }
    
    @Override
    public String toString() {
        return String.format(
                "Message[id=%d, sender='%s', receiver='%s', content='%s', time='%s']",
                id, sender, receiver, content, timeStamp);
    }

	public Long getId() {
		return id;
	}

	public User getSender() {
		return sender;
	}

	public User getReceiver() {
		return receiver;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public String getContent() {
		return content;
	}

	public AttachmentType getAttachment() {
		return attachment;
	}
	
	public Messenger getMessenger() {
		return messenger;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
