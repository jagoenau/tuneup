package se1app.applicationcore.interaction;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class RepeatingType implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String repeatingType;

	private Set<String> allowedRepeatingTypes = new HashSet<String>(
			Arrays.asList("once a week", "once a month", "once a year"));
	
	public RepeatingType(String repeatingType) {
		if (!allowedRepeatingTypes.contains(repeatingType)) {
			 throw new IllegalArgumentException("Not valid repeating type: " + repeatingType); 
		}
		this.repeatingType = repeatingType;
	}
	
	public String toString() {
		return repeatingType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((repeatingType == null) ? 0 : repeatingType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RepeatingType other = (RepeatingType) obj;
		if (repeatingType == null) {
			if (other.repeatingType != null)
				return false;
		} else if (!repeatingType.equals(other.repeatingType))
			return false;
		return true;
	}

}
