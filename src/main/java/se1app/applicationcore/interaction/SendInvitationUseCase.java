package se1app.applicationcore.interaction;

import se1app.applicationcore.profile.entities.User;

public interface SendInvitationUseCase {

    /**
     * Gets an event by the identifying id.
     *
     * @param id the identifying key for an event
     * @return the event if it was found
     */
    Event getEventById(Long id);

    /**
     * Gets an user by the identifying id.
     *
     * @param id the identifying key for an user
     * @return the user if it was found
     */
    User getUserById(Long id);

    /**
     * Create an invitation that can be sended to another user.
     *
     * @param event the event that the invitee is invited to
     * @param inviter the user that sends the invitation
     * @param invitee the user that gets invited
     * @param message the message that is attached to the invitation
     * @return an invitation object to send
     */
    Invitation createInvitation(Event event, User inviter, User invitee, String message);

    /**
     * Gets the messenger of a user.
     *
     * @param user the user the messenger belongs to
     * @return
     */
    Messenger getMessengerByUser(User user);

    /**
     * Sends an invitation to the invited user.
     *
     * @param invitation the invitation that shall be send.
     */
    void sendInvitation(Invitation invitation);

}
