package se1app.applicationcore.interaction;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import se1app.applicationcore.profile.entities.User;

@Entity
public class Messenger {
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToMany(cascade = CascadeType.ALL)
	private Set<User> users;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="messenger")
	private List<Message> messages;

    public Messenger(){}
    
    public Messenger(Set<User> users) {
    	this.users = users;
    }
    
	public String toString() {
		return String.format("Message[id=%d, messages='%s']", id, messages);
	}

	public Long getId() {
		return id;
	}

	public void addMessage(Message m) {
		messages.add(m);
	}
	
	public List<Message> getMessages() {
		return messages;
	}
	
	public Set<User> getUsers() {
		return users;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Messenger other = (Messenger) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
