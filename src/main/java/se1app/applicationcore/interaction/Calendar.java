package se1app.applicationcore.interaction;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import se1app.applicationcore.profile.entities.User;

@Entity
public class Calendar {
	@Id
	@GeneratedValue
	private Long id;
	
	@ManyToMany
	@JoinColumn(name = "events")
	private List<Event> events=new ArrayList<Event>();
	
	@OneToOne(cascade = CascadeType.ALL)
	private User user;
	
	public Calendar() {}
	
	public Calendar(User user) {
		this.user=user;
	}
	
	public String toString() {
        return String.format(
                "Calender[id=%d, user='%user', events='%events']",
                id, user, events);
	}

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}
	
	public void addEvent(Event event) {
		events.add(event);
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Calendar other = (Calendar) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
}
