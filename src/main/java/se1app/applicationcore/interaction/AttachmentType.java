package se1app.applicationcore.interaction;

import java.io.File;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class AttachmentType implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Set<String> acceptedDataType = new HashSet<String>(Arrays.asList(".mp3", ".pdf",".jpg",
			"gif", ".png", "mp4","avi"));
	
	private Object attachment;
	
	public AttachmentType(Invitation attachment) {
		this.attachment = attachment;
	}
	
	public AttachmentType(File attachment) {
	       String fileName = attachment.getName();  
	        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);  
	        if (!acceptedDataType.contains(suffix)) {
	        	throw new IllegalArgumentException("No valid file type:" + suffix);
	        }
	        this.attachment = attachment;
	}
	
	public Object getAttachment() {
		return attachment;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attachment == null) ? 0 : attachment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttachmentType other = (AttachmentType) obj;
		if (attachment == null) {
			if (other.attachment != null)
				return false;
		} else if (!attachment.equals(other.attachment))
			return false;
		return true;
	}
	
	
}
