package se1app.applicationcore.interaction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessengerRepository  extends JpaRepository<Messenger, Integer>{
	Messenger findByMessage(Message message);
}
