package se1app.applicationcore.interaction;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer> {
    Event findByInvitations(Invitation invitation);
    
    List<Event> findByCalenders(Calendar calendar);

    Event findById(long id);
}
