package se1app.applicationcore.interaction;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import se1app.applicationcore.profile.entities.User;

@Entity
public class Event {
	@Id
	@GeneratedValue
	private Long id;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy="event")
	private List<Invitation> invitations;
	
	@ManyToMany(mappedBy = "events")
	private Set<Calendar> calenders;
	
	private LocalDateTime start;
	private LocalDateTime end;
	
	@ManyToOne
	private User organizer;
	private String location;
	private String description;
	private RepeatingType repeating;
	
	public Event(String location, String description) {
		this.location = location;
		this.description = description;
	}
	
	
	
	public void setOrganizer(User organizer) {
		this.organizer = organizer;
	}



	public void addInvitation(Invitation invitation) {
		invitations.add(invitation);
	}
	
	public void setRepeatingType(RepeatingType repeating) {
		this.repeating = repeating;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", invitations=" + invitations + ", start=" + start + ", end=" + end + ", organizer="
				+ organizer + ", location=" + location + ", description=" + description + ", repeating=" + repeating + "]";
	}


	public LocalDateTime getStart() {
		return start;
	}

	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	public LocalDateTime getEnd() {
		return end;
	}

	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public RepeatingType getRepeating() {
		return repeating;
	}

	public void setRepeating(RepeatingType repeating) {
		this.repeating = repeating;
	}

	public Long getId() {
		return id;
	}

	public List<Invitation> getInvitations() {
		return invitations;
	}

	public User getOrganizer() {
		return organizer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Event other = (Event) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
