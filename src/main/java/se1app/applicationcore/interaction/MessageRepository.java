package se1app.applicationcore.interaction;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import se1app.applicationcore.profile.entities.User;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
	List<Message> findByMessenger(Messenger messenger);

//	Messenger findByUser(User user);
}
