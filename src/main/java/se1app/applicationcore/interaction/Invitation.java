package se1app.applicationcore.interaction;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import se1app.applicationcore.profile.entities.User;

@Entity
public class Invitation {
	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne 
	Event event;

	@ManyToOne(cascade = CascadeType.ALL)
	private User inviter;
	
	@ManyToOne(cascade = CascadeType.ALL)
	private User invitee;
	private String message;

	public Invitation(){}
	
	public Invitation(Event event, User inviter, User invitee) {
		this.event = event;
		this.inviter = inviter;
		this.invitee = invitee;
		this.message = "";
	}

	@Override
	public String toString() {
		return "Invitation [id=" + id + ", event=" + event + ", inviter=" + inviter + ", invitee=" + invitee
				+ ", message=" + message + "]";
	}

	public Long getId() {
		return id;
	}

	public Event getEvent() {
		return event;
	}

	public User getInviter() {
		return inviter;
	}

	public User getInvitee() {
		return invitee;
	}

	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invitation other = (Invitation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
