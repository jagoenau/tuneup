package se1app.applicationcore.interaction;

import org.springframework.beans.factory.annotation.Autowired;

import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.UserRepository;

public class SendInvitation implements SendInvitationUseCase {

    @Autowired
    UserRepository userRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    MessageRepository messageRepository;

    @Override
    public Event getEventById(Long id) {
        return eventRepository.findById(id);
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Invitation createInvitation(Event event, User inviter, User invitee, String message) {
        Invitation invitation = new Invitation(event, inviter, invitee);
        if (!message.isEmpty()) {
            invitation.setMessage(message);
        }
        return invitation;
    }

    @Override
    public Messenger getMessengerByUser(User user) {
        return messageRepository.findByUser(user);
    }

    @Override
    public void sendInvitation(Invitation invitation) {

        AttachmentType attachment = new AttachmentType(invitation);
        Message message = new Message(
                invitation.getInviter(),
                invitation.getInvitee(),
                invitation.getMessage(),
                attachment
        );
        Messenger messenger = getMessengerByUser(invitation.getInvitee());
        messenger.addMessage(message);

    }
}
