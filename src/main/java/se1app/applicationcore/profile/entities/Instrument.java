package se1app.applicationcore.profile.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import se1app.applicationcore.profile.dataTypes.InstrumentCategoryType;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Instrument {
    
    @Id
    @GeneratedValue
    private Integer id;
    
    @NotNull
    private String name;
    private InstrumentCategoryType category;
    
}
