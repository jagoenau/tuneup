package se1app.applicationcore.profile.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import se1app.applicationcore.groups.TimeStampType;

@Entity
public class Post {

	@Id
	@GeneratedValue
	private Integer id;

	@ManyToOne
	@NotNull
	private User author;

	@NotBlank
	private String content;
	
	private TimeStampType timeStamp;

	public Post(User author, String content) {
		this.author = author;
		this.content = content;
		timeStamp = TimeStampType.now();
	}

	public void editPost(String newContent) {
		if (newContent != null) {
			this.content = newContent;
		}
	}

	public User getAuthor() {
		return author;
	}

	public String getContent() {
		return content;
	}

	public TimeStampType getTimeStamp() {
		return timeStamp;
	}
	
	

	@Override
	public String toString() {
		return "Post [author: " + author + ", content: " + content + ", timeStamp: " + timeStamp + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Post other = (Post) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	

}
