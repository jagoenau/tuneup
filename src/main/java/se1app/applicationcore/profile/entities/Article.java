package se1app.applicationcore.profile.entities;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import se1app.applicationcore.profile.Visibility;
import se1app.applicationcore.profile.dataTypes.TagType;

@Entity
public class Article {
    
    @Id
    @GeneratedValue
    private Integer id;
    
    @ManyToOne(cascade = CascadeType.ALL)
    private User author;
    
    private String title;
    // TODO type of content
    private String content;
    private Visibility visibility = Visibility.PUBLIC;
    
    private LocalDateTime releaseDateTime = LocalDateTime.of(2016, 11, 14, 12, 55);
    
    @ElementCollection
    @Column(name = "tags")
    private List<TagType> tags = new ArrayList<>();
    
    public Article(String title, List<TagType> tags, String content, Visibility visibility,
        User author) {
        this.title = title;
        this.tags.addAll(tags);
        this.content = content;
        this.author = author;
        this.visibility = visibility;
        
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public Visibility getVisibility() {
        return this.visibility;
    }
    
    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }
    
    public String getContent() {
        return content;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
    
    public LocalDateTime getReleaseTime() {
        return releaseDateTime;
    }
    
    public void setReleaseTime(LocalDateTime releaseTime) {
        this.releaseDateTime = releaseTime;
    }
    
    public User getAuthor() {
        return this.author;
    }
    
    public void setAuthor(User author) {
        this.author = author;
    }
    
    public List<TagType> getTags() {
        return tags;
    }
    
    public void addTag(TagType tag) {
        tags.add(tag);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((releaseDateTime == null) ? 0 : releaseDateTime.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        result = prime * result + ((visibility == null) ? 0 : visibility.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Article other = (Article) obj;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (releaseDateTime == null) {
            if (other.releaseDateTime != null)
                return false;
        } else if (!releaseDateTime.equals(other.releaseDateTime))
            return false;
        if (tags == null) {
            if (other.tags != null)
                return false;
        } else if (!tags.equals(other.tags))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        if (visibility != other.visibility)
            return false;
        return true;
    }
    
}
