package se1app.applicationcore.profile.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import se1app.applicationcore.profile.dataTypes.EmailType;
import se1app.applicationcore.profile.dataTypes.PasswordType;

@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class User {
    
    @Id
    @GeneratedValue
    private Integer id;
    
    @NotNull
    private String name;
    
    @OneToOne(cascade = CascadeType.ALL)
    private Player player;
    
    // Fachliche Datentypen (immutable)
    private EmailType email;
    private PasswordType password;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_articles")
    private List<Article> articles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_instruments")
    private List<Instrument> instruments = new ArrayList<>();
    
    public User(String name, EmailType mail, PasswordType password) {
        this.name = name;
        this.email = mail;
        this.password = password;
        
    }
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public void setPlayer(Player player) {
        this.player = player;
    }
    
    public EmailType getEmail() {
        return email;
    }
    
    public void setEmail(EmailType email) {
        this.email = email;
    }
    
    public PasswordType getPassword() {
        return password;
    }
    
    public void setPassword(PasswordType password) {
        this.password = password;
    }
    
    public List<Article> getArticles() {
        return articles;
    }
    
    public void addArticle(Article article) {
        articles.add(article);
    }
    
    public List<Instrument> getInstruments() {
        return instruments;
    }
    
    public void addInstrument(Instrument instrument) {
        instruments.add(instrument);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (email == null) {
            if (other.email != null)
                return false;
        } else if (!email.equals(other.email))
            return false;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
    
}
