package se1app.applicationcore.profile.dataTypes;

import java.io.Serializable;

/**
 * save Passwords have to have at least one Upper and one lower Case character +
 * at least one digit. It has to be 8 - 20 characters long
 * 
 * @author Mimi
 */
public class PasswordType implements Serializable {
    
    // @see
    // http://regexlib.com/Search.aspx?k=password&AspxAutoDetectCookieSupport=1
    private static final String PASSWORD_PATTERN = "^[a-zA-Z0-9@\\\\#$%&*()_+\\]\\[';:?.,!^-]{8,}$";
    private final String p;
    
    public PasswordType(String password) {
        if (!isValidPassword(password)) {
            throw new IllegalArgumentException("Kein gültiges Passwort: " + password);
        }
        this.p = password;
    }
    
    public static boolean isValidPassword(String password) {
        return password.matches(PASSWORD_PATTERN);
    }
    
    public String getPassword() {
        return p;
    }
    
    @Override
    public int hashCode() {
        final int prime = 37;
        int result = 1;
        result = prime * result + p.hashCode();
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PasswordType other = (PasswordType) obj;
        if (p == null) {
            if (other.p != null)
                return false;
        } else if (!p.equals(other.p)) {
            return false;
        }
        return true;
    }
}
