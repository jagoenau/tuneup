package se1app.applicationcore.profile.dataTypes;

import java.io.Serializable;

public class TagType implements Serializable {
    
    private static final String TAG_PATTERN = "[A-Za-z0-9_äÄöÖüÜß]+";
    
    private String tag;
    
    public TagType(String tag) {
        if (!isValidTag(tag)) {
            throw new IllegalArgumentException("Kein gültiger Tag: " + tag);
        }
        this.tag = tag;
    }
    
    public String getTag() {
        return this.tag;
    }
    
    public boolean isValidTag(String tag) {
        return tag.matches(TAG_PATTERN);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((tag == null) ? 0 : tag.hashCode());
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TagType other = (TagType) obj;
        if (tag == null) {
            if (other.tag != null)
                return false;
        } else if (!tag.equals(other.tag))
            return false;
        return true;
    }
    
}
