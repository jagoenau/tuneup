package se1app.applicationcore.profile.repositories;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import se1app.applicationcore.profile.Visibility;
import se1app.applicationcore.profile.entities.Article;
import se1app.applicationcore.profile.entities.User;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {
    
    Optional<Article> findByTitle(String title);
    
    /**
     * hier nochmal ein neuer versuch um auf Datenbenkebene auf die Liste der Tags zuzugreifen.
     * Leider konnte ich die Anfrage nicht mehr testen, da unsere Partnergruppen einige Änderungen 
     * an ihrem Teil im Projekt gemacht haben und sich seitdem die Applikation nicht starten lässt.
     * @param tag
     * @return
     */
    @Query(value = "SELECT a FROM ARTICLE a JOIN a.tags t WHERE t:= 'tag' )", nativeQuery = true)
    List<Article> findByTags(@Param("tag") String tag);
    
    List<Article> findByReleaseDateTime(LocalDateTime releasedatetime);
    
    List<Article> findByAuthor(User author);
    
    List<Article> findByVisibility(Visibility visibility);
    
}