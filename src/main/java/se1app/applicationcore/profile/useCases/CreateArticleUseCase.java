package se1app.applicationcore.profile.useCases;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import se1app.applicationcore.profile.Visibility;
import se1app.applicationcore.profile.dataTypes.TagType;
import se1app.applicationcore.profile.entities.Article;
import se1app.applicationcore.profile.entities.User;
import se1app.applicationcore.profile.repositories.ArticleRepository;
import se1app.applicationcore.profile.repositories.UserRepository;

public class CreateArticleUseCase implements CreateArticleUseCaseInterface {
    
    @Autowired
    ArticleRepository articleRepository;
    @Autowired
    UserRepository userRepository;
    
    @Override
    public List<Article> getArticlesByUser(User user) throws IllegalArgumentException {
        if (user == null) {
            throw new IllegalArgumentException("no user is given");
        } else {
            return articleRepository.findByAuthor(user);
        }
    }
    
    @Override
    public Article publishArticle(String title, List<TagType> tags,
        String content, Visibility visibility, User author) throws IllegalArgumentException {
        if (title == null) {
            throw new IllegalArgumentException("Please choose a title");
        } else if (tags.size() < 3) {
            throw new IllegalArgumentException("Please choose more than 3 tags");
        } else if (content == null) {
            throw new IllegalArgumentException("You can´t publish an empty article");
        } else if (author == null) {
            throw new IllegalArgumentException("You can´t publish an empty article");
        } else {
            Article article = new Article(title, tags, content, visibility, author);
            author.addArticle(article);
            articleRepository.save(article);
            return article;
        }
    }
    
}
