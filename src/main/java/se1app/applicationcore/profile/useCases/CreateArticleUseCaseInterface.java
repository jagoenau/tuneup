package se1app.applicationcore.profile.useCases;

import java.util.List;

import se1app.applicationcore.profile.Visibility;
import se1app.applicationcore.profile.dataTypes.TagType;
import se1app.applicationcore.profile.entities.Article;
import se1app.applicationcore.profile.entities.User;

/**
 * @author Mieke Narjes - David Hoeck
 * @throws IllegalArgumentException
 *             if no user is given.
 * @return list of Articles, matching the given tag. Most of the operations to
 *         create a new article are handled in the ui part of the application.
 *         The only Systemoperation is the publish Operation.
 */
public interface CreateArticleUseCaseInterface {
    
    /**
     * The method gets all Articles of a specific user.
     * 
     * @param user
     * @return all articles of the given user.
     * @throws IllegalArgumentException
     *             if user is null
     */
    List<Article> getArticlesByUser(User user);
    
    /**
     * The publishArticle() operation creates a new article. It gets the
     * following parameters:
     * 
     * @param title
     *            of the article
     * @param tags
     *            to organize and find the article
     * @param visibilty
     *            ( public , friends , private )
     * @param content
     *            of the article (maybe other data type then string?)
     * @param user
     *            who created the article
     * @throws IllegalArgumentException
     *             if no user is given, less then 3 tags are given or no content
     *             is given.
     * @return the published article.
     */
    Article publishArticle(String title, List<TagType> tags, String text, Visibility visibilty,
        User author);
    
}
