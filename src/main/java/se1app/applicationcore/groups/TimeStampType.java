package se1app.applicationcore.groups;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * A data type representing a timestamp. This class implements the Serializable
 * Interface so Objects of this type can be saved in the database.
 * 
 * @author Lisa Ahlers
 *
 */
public class TimeStampType implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5117795384710015997L;
	private LocalDate date;
	private LocalTime time;

	public TimeStampType(LocalDate date, LocalTime time) {
		this.date = date;
		this.time = time;
	}

	public static TimeStampType now() {
		LocalDateTime now = LocalDateTime.now();
		return new TimeStampType(LocalDate.of(now.getYear(), now.getMonth(), now.getDayOfMonth()),
				LocalTime.of(now.getHour(), now.getMinute(), now.getSecond()));
	}

	public LocalDate getDate() {
		return date;
	}

	public LocalTime getTime() {
		return time;
	}

	@Override
	public String toString() {
		return "TimeStamp: " + date + "at " + time;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeStampType other = (TimeStampType) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		return true;
	}

}
