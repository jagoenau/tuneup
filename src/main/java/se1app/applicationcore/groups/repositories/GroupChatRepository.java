package se1app.applicationcore.groups.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import se1app.applicationcore.groups.entities.ChatMessage;
import se1app.applicationcore.groups.entities.GroupChat;

public interface GroupChatRepository extends JpaRepository<GroupChat, Integer> {

	List<GroupChat> findByMessages(List<ChatMessage> messages);

}
