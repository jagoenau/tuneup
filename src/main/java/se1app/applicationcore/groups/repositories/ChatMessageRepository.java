package se1app.applicationcore.groups.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import se1app.applicationcore.groups.TimeStampType;
import se1app.applicationcore.groups.entities.ChatMessage;
import se1app.applicationcore.profile.entities.User;

public interface ChatMessageRepository extends JpaRepository<ChatMessage, Integer>{

	List<ChatMessage> findBySender(User Sender);

	List<ChatMessage> findByTimeStamp(TimeStampType timeStamp);
	
	List<ChatMessage> findByContentContaining(String content);
}
