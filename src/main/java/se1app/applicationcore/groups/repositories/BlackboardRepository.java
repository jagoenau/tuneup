package se1app.applicationcore.groups.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import se1app.applicationcore.groups.entities.Blackboard;
import se1app.applicationcore.profile.entities.Post;

public interface BlackboardRepository extends JpaRepository<Blackboard, Integer> {

	List<Blackboard> findByPosts(List<Post> posts);
}
