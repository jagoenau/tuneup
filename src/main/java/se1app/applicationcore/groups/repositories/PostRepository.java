package se1app.applicationcore.groups.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import se1app.applicationcore.groups.TimeStampType;
import se1app.applicationcore.profile.entities.Post;
import se1app.applicationcore.profile.entities.User;

public interface PostRepository extends JpaRepository<Post, Integer> {

	List<Post> findByAuthor(User author);

	List<Post> findByTimeStamp(TimeStampType timeStamp);
	
	List<Post> findByContentContaining(String text);

}
