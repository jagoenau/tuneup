package se1app.applicationcore.groups.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import se1app.applicationcore.groups.entities.Crowd;
import se1app.applicationcore.profile.entities.User;

@Repository
public interface CrowdRepository extends JpaRepository<Crowd, Integer> {
	
	List<Crowd> findByNameIgnoreCaseContaining(String name);
	
	List<Crowd> findByMembers(List<User> members);

}
