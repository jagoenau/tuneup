package se1app.applicationcore.groups.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import se1app.applicationcore.groups.GenreType;
import se1app.applicationcore.groups.entities.Band;
import se1app.applicationcore.profile.entities.Instrument;
import se1app.applicationcore.profile.entities.User;

public interface BandRepository extends JpaRepository<Band, Integer> {
	List<Band> findByNameIgnoreCaseContaining(String name);

	List<Band> findByMembers(List<User> members);

	List<Band> findByGenres(List<GenreType> genres);

	List<Band> findByInstruments(List<Instrument> instruments);
}
