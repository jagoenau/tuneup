package se1app.applicationcore.groups.usecase;

import java.util.List;

import se1app.applicationcore.groups.MaximumGroupSizeReachedException;
import se1app.applicationcore.groups.entities.Crowd;
import se1app.applicationcore.profile.entities.User;

public interface CrowdCreation {

	/**
	 * Finds all Crowds this user is part of.
	 * 
	 * @param user
	 *            The user whose crowds shall be returned.
	 * @return A list of all crowds the user is part of.
	 */
	List<Crowd> getCrowdsForUser(User user);

	/**
	 * Creates a new Crowd with the given parameters and returns it.
	 * 
	 * @param owner
	 *            The user that creates the new Crowd. This user is a member and
	 *            a moderator of this Crowd by default. Can't be null.
	 * @param name
	 *            The name of the new Crowd. Can't be null.
	 * @param infoText
	 *            The info text for the new Crowd. Can't be null.
	 * @param members
	 *            A list of initial members for the new Crowd. If this parameter
	 *            is null the crowd won't have any default members except for
	 *            the user that created the Crowd.
	 * @return the new Crowd
	 * @throws MaximumGroupSizeReachedException
	 *             If the initial members are more than the maximum group size.
	 */
	Crowd createCrowd(User owner, String name, String infoText, List<User> members)
			throws MaximumGroupSizeReachedException;

	/**
	 * Adds the given Users to the moderators of the Crowd. Users are only added
	 * as moderators if they are already member of this Crowd. So users in the
	 * add List that aren't members of the Crowd won't be added to the
	 * moderators and a {@link IllegalArgumentException} will be thrown.
	 * 
	 * @param crowd
	 *            The crowd that should be edited. Can't be null.
	 * @param add
	 *            A list of users that should become moderators of the crowd.
	 */
	void addModerators(Crowd crowd, List<User> add);

	/**
	 * Removes the given Users from the moderators of the Crowd. These users
	 * will still be members of the Crowd.
	 * 
	 * @param crowd
	 *            The crowd that should be edited. Can't be null.
	 * @param remove
	 *            A list of users that should be removed from the moderators of
	 *            the crowd.
	 */
	void removeModerators(Crowd crowd, List<User> remove);

	/**
	 * Invites the given user to the given crowd which means that the user will
	 * be added to the crowd.
	 * 
	 * @param user
	 *            The user that should be added.
	 * @param crowd
	 *            The crowd that should be edited. Can't be null.
	 * @throws MaximumGroupSizeReachedException
	 *             when the maximum Crowd size is reached.
	 */
	void inviteUser(User user, Crowd crowd) throws MaximumGroupSizeReachedException;

	/**
	 * Invites multiple users at once. (Adds multiple users to the crowd at
	 * once).
	 * 
	 * @param users
	 *            The users that should be added to the crowd.
	 * @param crowd
	 *            The crowd that should be edited. Can't be null.
	 * @throws MaximumGroupSizeReachedException
	 *             when the maximum Crowd size is reached by adding the given
	 *             users.
	 */
	void inviteUsers(List<User> users, Crowd crowd) throws MaximumGroupSizeReachedException;

}
