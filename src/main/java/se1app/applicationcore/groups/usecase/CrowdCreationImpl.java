package se1app.applicationcore.groups.usecase;

import java.util.Arrays;
import java.util.List;

import se1app.applicationcore.groups.MaximumGroupSizeReachedException;
import se1app.applicationcore.groups.entities.Crowd;
import se1app.applicationcore.groups.repositories.CrowdRepository;
import se1app.applicationcore.profile.entities.User;

public class CrowdCreationImpl implements CrowdCreation {

	CrowdRepository crowdRepository;

	public CrowdCreationImpl(CrowdRepository crowdRepository) {
		this.crowdRepository = crowdRepository;
	}

	@Override
	public List<Crowd> getCrowdsForUser(User user) {
		if (user != null) {
			return crowdRepository.findByMembers(Arrays.asList(user));
		} else {
			throw new IllegalArgumentException("The given user can't be null!");
		}
	}

	@Override
	public Crowd createCrowd(User owner, String name, String infoText, List<User> members)
			throws MaximumGroupSizeReachedException {
		if (owner == null) {
			throw new IllegalArgumentException("The owner of a Crowd can't be null");
		} else if (infoText == null) {
			throw new IllegalArgumentException("The info text can't be null!");
		} else if (name == null) {
			throw new IllegalArgumentException("The name of the crowd can't be null!");
		} else {
			return create(owner, name, infoText, members);
		}
	}

	private Crowd create(User owner, String name, String infoText, List<User> members)
			throws MaximumGroupSizeReachedException {
		Crowd newCrowd;
		if (members != null) {
			newCrowd = new Crowd(name, infoText, members);
		} else {
			newCrowd = new Crowd(name);
			newCrowd.setInfo(infoText);
		}
		// The owner is moderator and member by default.
		newCrowd.addMember(owner);
		newCrowd.addModerator(owner);

		assert newCrowd != null;
		return crowdRepository.save(newCrowd);
	}

	@Override
	public void addModerators(Crowd crowd, List<User> add) {
		if (crowd == null) {
			throw new IllegalArgumentException("The given crowd can't be null!");
		} else if (add == null) {
			throw new IllegalArgumentException("The given list of users can't be null!");
		} else {
			for (User user : add) {
				crowd.addModerator(user);
			}
			crowdRepository.save(crowd);
		}
	}

	@Override
	public void removeModerators(Crowd crowd, List<User> remove) {
		if (crowd == null) {
			throw new IllegalArgumentException("The given crowd can't be null!");
		} else if (remove == null) {
			throw new IllegalArgumentException("The given list of users can't be null!");
		} else {
			for (User user : remove) {
				crowd.removeModerator(user);
			}
			crowdRepository.save(crowd);
		}
	}

	@Override
	public void inviteUser(User user, Crowd crowd) throws MaximumGroupSizeReachedException {
		if (crowd != null) {
			crowd.addMember(user);
			crowdRepository.save(crowd);
		} else {
			throw new IllegalArgumentException("The given crowd can't be null!");
		}
	}

	@Override
	public void inviteUsers(List<User> users, Crowd crowd) throws MaximumGroupSizeReachedException {
		if (crowd == null) {
			throw new IllegalArgumentException("The given crowd can't be null!");
		} else if (users == null) {
			throw new IllegalArgumentException("The given list of users can't be null!");
		} else {
			for (User user : users) {
				crowd.addMember(user);
			}
			crowdRepository.save(crowd);
		}
	}

}
