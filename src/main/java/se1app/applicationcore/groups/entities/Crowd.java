package se1app.applicationcore.groups.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotBlank;

import se1app.applicationcore.groups.MaximumGroupSizeReachedException;
import se1app.applicationcore.profile.entities.User;

/**
 * This entity represents a crowd, a type of group with upto 1000 members and a
 * basic communication tool(a blackboard).
 * 
 * @author Lisa Ahlers
 *
 */
@Entity
public class Crowd {

	private static final int MAX_SIZE = 1000;

	@Id
	@GeneratedValue
	private Integer id;

	@NotBlank
	String name;
	String info;

	@ManyToMany(cascade = CascadeType.PERSIST)
	List<User> members;

	@ManyToMany(cascade = CascadeType.PERSIST)
	List<User> moderators;

	@OneToOne(cascade = CascadeType.ALL)
	Blackboard blackboard;

	public Crowd(String name) {
		this.name = name;
		blackboard = new Blackboard();
		moderators = new ArrayList<>();
		members = new ArrayList<>();
	}

	public Crowd(String name, String info, List<User> initialInvites) throws MaximumGroupSizeReachedException {
		this.name = name;
		this.info = info;
		blackboard = new Blackboard();
		moderators = new ArrayList<>();
		if (initialInvites.size() <= MAX_SIZE) {
			members = initialInvites;
		} else {
			throw new MaximumGroupSizeReachedException();
		}
	}

	public String getName() {
		return name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public List<User> getMembers() {
		return members;
	}

	/**
	 * Adds a member to the Crowd if the maximum Crowd size isn't reached.
	 * 
	 * @param newMember
	 * @throws MaximumGroupSizeReachedException
	 */
	public void addMember(User newMember) throws MaximumGroupSizeReachedException {
		if (members.size() < MAX_SIZE) {
			if (newMember != null && !members.contains(newMember)) {
				members.add(newMember);
			}
		} else {
			throw new MaximumGroupSizeReachedException();
		}
	}

	public void removeMember(User member) {
		if (member != null) {
			members.remove(member);
		} else {
			throw new IllegalArgumentException("The user can't be null!");
		}
	}

	public List<User> getModerators() {
		return moderators;
	}

	/**
	 * Adds a User as a moderator to this Crowd but only if he is already a
	 * member of the Crowd.
	 * 
	 * @param moderator
	 */
	public void addModerator(User moderator) {
		if (moderator != null) {
			if (members.contains(moderator)) {
				moderators.add(moderator);
			} else {
				throw new IllegalArgumentException("User isn't a member of this crowd!");
			}
		} else {
			throw new IllegalArgumentException("The user can't be null!");
		}
	}

	public void removeModerator(User moderator) {
		if (moderator != null) {
			moderators.remove(moderator);
		} else {
			throw new IllegalArgumentException("The user can't be null!");
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Crowd other = (Crowd) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
