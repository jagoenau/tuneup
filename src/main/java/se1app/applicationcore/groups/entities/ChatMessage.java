package se1app.applicationcore.groups.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import se1app.applicationcore.groups.TimeStampType;
import se1app.applicationcore.profile.entities.User;

@Entity
public class ChatMessage {

	@Id
	@GeneratedValue
	Integer id;

	@NotNull
	@ManyToOne
	User sender;

	private TimeStampType timeStamp;
	@NotBlank
	private String content;

	public ChatMessage(User sender, String content) {
		this.sender = sender;
		this.content = content;
		timeStamp = TimeStampType.now();
	}

	public String getContent() {
		return content;
	}

	public User getSender() {
		return sender;
	}

	public TimeStampType getTimeStamp() {
		return timeStamp;
	}

	@Override
	public String toString() {
		return "ChatMessage [sender: " + sender + ", timeStamp: " + timeStamp + ", content: " + content + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChatMessage other = (ChatMessage) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
