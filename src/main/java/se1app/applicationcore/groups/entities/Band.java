package se1app.applicationcore.groups.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotBlank;

import se1app.applicationcore.groups.GenreType;
import se1app.applicationcore.groups.MaximumGroupSizeReachedException;
import se1app.applicationcore.interaction.Calendar;
import se1app.applicationcore.profile.entities.Instrument;
import se1app.applicationcore.profile.entities.Player;
import se1app.applicationcore.profile.entities.User;

@Entity
public class Band {
	@Id
	@GeneratedValue
	private Integer id;

	@NotBlank
	String name;

	String info;

	@ElementCollection(targetClass = GenreType.class)
	Set<GenreType> genres;

	@ManyToMany(cascade = CascadeType.PERSIST)
	List<User> members;

	@OneToOne(cascade = CascadeType.ALL)
	Blackboard blackboard;

	@OneToOne(cascade = CascadeType.ALL)
	private Calendar calendar;

	@OneToOne(cascade = CascadeType.ALL)
	private GroupChat chat;

	@OneToOne(cascade = CascadeType.ALL)
	private Player player;

	@ManyToMany
	private List<Instrument> instruments;

	public Band(String name, List<User> members) {
		this.name = name;
		this.members = members;
		genres = new HashSet<>();
		blackboard = new Blackboard();
		calendar = new Calendar();
		chat = new GroupChat();
		player = new Player();
		instruments = new ArrayList<>();
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getName() {
		return name;
	}

	public List<User> getMembers() {
		return members;
	}

	public void addMember(User newMember, Instrument instrument) throws MaximumGroupSizeReachedException {
		if (members.size() < 50) {
			members.add(newMember);
			instruments.add(instrument);
		}else{
			throw new MaximumGroupSizeReachedException();
		}
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public GroupChat getChat() {
		return chat;
	}

	public Player getMediaPlayer() {
		return player;
	}

	public List<Instrument> getInstruments() {
		return instruments;
	}

	public Set<GenreType> getGenres() {
		return genres;
	}

	public void addGenre(GenreType genre) {
		genres.add(genre);
	}

	public void removeGenre(GenreType genre) {
		genres.remove(genre);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Band other = (Band) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("Band:[id=%d, name='%s']", id, name);
	}

}
