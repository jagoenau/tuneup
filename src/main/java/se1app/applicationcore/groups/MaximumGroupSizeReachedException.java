package se1app.applicationcore.groups;

/**
 * This Exception is used when the maximum size of any type of group is reached.
 * 
 * @author Lisa Ahlers
 *
 */
public class MaximumGroupSizeReachedException extends Exception {

	private static final long serialVersionUID = 1L;

}
