package se1app.applicationcore.groups;

/**
 * This Enum represents different music genres.
 * 
 * @author Lisa Ahlers
 *
 */
public enum GenreType {
	HARDROCK("Hard Rock"), ROCK("Rock"), METAL("Metal"), POP("Pop"), RAP("Rap"), //
	CLASSICAL("Classical"), BLUES("Blues"), ELECTRONIC("Electronic"), //
	LATIN("Latin"), JAZZ("Jazz"), COUNTRY("Country"), INSTRUMENTAL("Instrumental");

	private final String stringRepresentation;

	GenreType(String stringRepresentation) {
		this.stringRepresentation = stringRepresentation;
	}

	public String toString() {
		return stringRepresentation;
	}

}
